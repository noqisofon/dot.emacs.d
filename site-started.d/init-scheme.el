;;; init-scheme.el --- 

;; Copyright (C) 2014  ned rihine

;; Author: ned rihine <ned.rihine@gmail.com>
;; Keywords: tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:
(setq quack-default-program "csi")

(require-if-exists quack)
;; (require-if-exists cluck)
(require-if-exists chicken)
(require-if-exists scheme-complete)

(add-hook 'scheme-mode-hook (lambda ()
                              (make-local-variable 'eldoc-documentation-function)
                              (setq lisp-indent-function 'scheme-smart-indent-function)
                              ;; (setq lisp-body-indent 4)
                              (setq eldoc-documentation-function 'scheme-get-current-symbol-info)
                              (eldoc-mode)))

(provide 'init-scheme)
;;; init-scheme.el ends here
